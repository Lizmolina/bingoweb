# BingoWeb

1. dotnet new angular -o BingoWeb
2. dotnet add package Microsoft.EntityFrameworkCore.SqlServer
3. dotnet add package Microsoft.EntityFrameworkCore.InMemory
4. dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer
5. dotnet add package System.IdentityModel.Tokens.Jwt
6. dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
7. dotnet add package Microsoft.EntityFrameworkCore.Design
8. dotnet add package Npgsql
9. dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL
10.  dotnet tool install --global dotnet-ef
11. dotnet ef migrations add InitialCreate
12. dotnet ef database update
13. https://cli.angular.io/
14. npm install -g @angular/cli en la carpeta ClientApp
15. ng g component games --skip-import