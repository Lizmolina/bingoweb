﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BingoWeb.Models
{
    public class Game
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set;}
        public string Link { get; set; }
        public bool  Status { get; set; }

    }
}
