using System;
using System.ComponentModel.DataAnnotations;

namespace BingoWeb.Models
{
    public class Paper
    {
        [Key]
        public long IdPartida { get; set; }
        public string TypePaper { get; set; }
        
    }
}
